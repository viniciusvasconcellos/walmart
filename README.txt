estrutura.
A dinificao da estrutura do projeto foi feita a partir da disponibilidade de recursos de infraestrutura. Foram utilizadas as linguagens 
de programacao de script de servidor PHP para gerar os recursos de webservice e a implementacao do banco de dados SQL para manter os dados
persistidos. O servidor utilizando e suas tecnologias s�o open source no caso Apache e PHP. Os recursos foram hospedados em um servidor 
externo para poderem ser homologados externamentes ao ambiente de desenvolvimento.

Foram criadas 3 tabelas no banco de dados: mapas, rotas e requisicoes, onde armazenam e cruzam as informacoes de rotas e requisicoes efetuadas
para determiando mapa.

O webservice no caso foi desenvolvido utilizando a biblioteca nusoap do php em um arquivo de script chamado server.php
 
escopo.
As informacoes sao armazendas na base e chamadas para interface do usuario atraves da linguagem javascript. 
Atraves dela podem ser inseridas e recuperadas as informacoes respectivas a um dos mapas existentes ou de um novo mapa.
O calculo de rota, bem como as funcinalidades de insercao de informacoes e consulta na base de dados sao feitos a nivel 
de script de servidor, localizado no proprio arquivo do webservice onde sao declarados os metodos, todas as requisicoes tambem sao armazenadas.
 
superficie & interface.
A interface disponibiliza ao usuario recursos de input de informacoes e selecao de mapas para recuperacao de informacoes relacionadas as rotas.
atraves dos campos de formulario o usuario insere informacoes sobre origem e destino de rotas para serem calculados os custos e distancias totais
a partir das informacoes de autonomia e valor de combustivel.

Estao disponibilizados em servidores de hospedagem (no caso utilizado um dominio de um projeto ja existente).
Estao localizado em diferentes ambientes para finalidade de testes com recursos de cross-origem, onde um ambiente externo possa utilizar
recursos de um outro ambiente hospedado.


webservice: http://www.hellofarma.com.br/walmart/server.php?wsdl
interface: http://www.viniciusvasconcellos.com.br/walmart/interface.html
testes de calculo: http://www.hellofarma.com.br/walmart/calculoRota.php
repositorio: https://bitbucket.org/viniciusvasconcellos/walmart


--- funcoes webservice
insereMapa:
parametro tipo string para o nome do mapa

carregaRotas:
parametro tipo string com o nome do mapa para carregar rotas especificas

insereRota:
parametro string com os valores nome do mapa, origem, destino, distancia separados por ','

calculaRota:
parametro string com os valores nome do mapa, origem, destino, autonomia, valor separados por ';'

carregaRequisicoes:
parametro tipo string com o nome do mapa para carregar requisicoes especificas

