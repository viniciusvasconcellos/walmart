<?php
	
	
	$serverName 	= 	"VINICIUS-PC\SQLEXPRESS"; 
	
	date_default_timezone_set("America/Sao_Paulo");

	require_once('lib/nusoap.php');
	
	$server		=	new	soap_server;
	
	$server->configureWSDL('server.entregandoMercadorias', 'urn:server.entregandoMercadorias');
	$server->wsdl->schemaTargetNamespace	=	'urn:server.entregandoMercadorias';
	
	$server->register('insereMapa', array('a' => 'xsd:string'), array('return' => 'xsd:string'), 'urn:server.insereMapa', 'urn:server.insereMapa#insereMapa', 'rpc', 'encoded', 'Insere Mapa');
	$server->register('carregaMapas', array('a' => 'xsd:string'), array('return' => 'xsd:string'), 'urn:server.carregaMapas', 'urn:server.carregaMapas#carregaMapas', 'rpc', 'encoded', 'carregaMapas');
	$server->register('carregaRotas', array('a' => 'xsd:string'), array('return' => 'xsd:string'), 'urn:server.carregaRotas', 'urn:server.carregaRotas#carregaRotas', 'rpc', 'encoded', 'carrega Rotas');
	$server->register('insereRota', array('a' => 'xsd:string'), array('return' => 'xsd:string'), 'urn:server.insereRota', 'urn:server.insereRota#insereRota', 'rpc', 'encoded', 'insere Rotas');
	$server->register('calculaRota', array('a' => 'xsd:string'), array('return' => 'xsd:string'), 'urn:server.calculaRota', 'urn:server.calculaRota#calculaRota', 'rpc', 'encoded', 'calcula Rota');
	$server->register('carregaRequisicoes', array('a' => 'xsd:string'), array('return' => 'xsd:string'), 'urn:server.carregaRequisicoes', 'urn:server.carregaRequisicoes#carregaRequisicoes', 'rpc', 'encoded', 'carrega Requisicoes');
	

	///  função utilizada para cadastrar novos mapas na base de dados.
	function insereMapa($a) {
		
		$options 		=	array( "Scrollable" => SQLSRV_CURSOR_KEYSET );

		if ($_SERVER['SERVER_NAME'] == 'localhost') 
		{
		
			$serverName 	= 	"VINICIUS-PC\SQLEXPRESS"; 		
			$connectionInfo = 	array( "Database"=>"walmart", "UID"=>"sa", "PWD"=>"m3scl4d0");
			$conn			=	sqlsrv_connect($serverName, $connectionInfo);
			
		} else 
		{
		
			$serverName 	= 	"187.18.58.83"; 	   
			$connectionInfo =	array( "Database"=>"05226_01", "UID"=>"hellofarma_adm", "PWD"=>"a6hP6b4jC1");		
			$conn 			= 	sqlsrv_connect($serverName, $connectionInfo);
			
		}
		
	
		$strSQL		=	"INSERT INTO mapas(nome) values(?) ";
		$pars		=	array($a);
		
		$stmt		=	sqlsrv_query($conn, $strSQL, $pars, $options) or die(print_r(sqlsrv_errors()));
		
		return 'sucesso';	
		
	}
	///  função utilizada para carregar os nomes dos mapas cadastrados.
	function carregaMapas($a)
	{
		$options 		=	array( "Scrollable" => SQLSRV_CURSOR_KEYSET );

		if ($_SERVER['SERVER_NAME'] == 'localhost') 
		{
		
			$serverName 	= 	"VINICIUS-PC\SQLEXPRESS"; 		
			$connectionInfo = 	array( "Database"=>"walmart", "UID"=>"sa", "PWD"=>"m3scl4d0");
			$conn			=	sqlsrv_connect($serverName, $connectionInfo);
			
		} else 
		{
		
			$serverName 	= 	"187.18.58.83"; 	   
			$connectionInfo =	array( "Database"=>"05226_01", "UID"=>"hellofarma_adm", "PWD"=>"a6hP6b4jC1");		
			$conn 			= 	sqlsrv_connect($serverName, $connectionInfo);
			
		}
		
	
		$strSQL			=	"	Select * from mapas ";
		$pars			=	array();
		
		$stmt			=	sqlsrv_query($conn, $strSQL, $pars, $options) or die(print_r(sqlsrv_errors()));
		
		$retorno		=	'';
		
		while ($obj		=	sqlsrv_fetch_object($stmt))
		{
			$retorno	.=	$obj->nome.',';
		}
		
		$retorno		=	substr($retorno, 0, strlen($retorno) - 1);
		
		return $retorno;	
		
	}
	///  função utilizada para carregar as rotas dos mapas especificos.
	function carregaRotas($a) {
		
		$options 		=	array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		
		if ($_SERVER['SERVER_NAME'] == 'localhost') 
		{
		
			$serverName 	= 	"VINICIUS-PC\SQLEXPRESS"; 		
			$connectionInfo = 	array( "Database"=>"walmart", "UID"=>"sa", "PWD"=>"m3scl4d0");
			$conn			=	sqlsrv_connect($serverName, $connectionInfo);
			
		} else 
		{
		
			$serverName 	= 	"187.18.58.83"; 	   
			$connectionInfo =	array( "Database"=>"05226_01", "UID"=>"hellofarma_adm", "PWD"=>"a6hP6b4jC1");		
			$conn 			= 	sqlsrv_connect($serverName, $connectionInfo);
			
		}
		
		$strSQL			=	"SELECT * FROM mapas WHERE nome = ? ";
		$pars			=	array($a);
		$stmt			=	sqlsrv_query($conn, $strSQL, $pars, $options) or die(print_r(sqlsrv_errors()));
		$obj			=	sqlsrv_fetch_object($stmt);
		
		
		$strSQL			=	"	SELECT * FROM rotas where mapa = ? ";
		$pars			=	array($obj->id);
		
		$stmt			=	sqlsrv_query($conn, $strSQL, $pars, $options) or die(print_r(sqlsrv_errors()));
		
		$retorno		=	'';
		
		while ($obj		=	sqlsrv_fetch_object($stmt))
		{
			$retorno	.=	$obj->origem.' '.$obj->destino.' '.$obj->distancia.',';
		}
		
		$retorno		=	substr($retorno, 0, strlen($retorno) - 1);
		
		return $retorno;	
		
	}
	///  função utilizada para cadastrar nova rota de uma mapa especifico. 
	function insereRota($a) {
		
		$options 		=	array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		
		if ($_SERVER['SERVER_NAME'] == 'localhost') 
		{
		
			$serverName 	= 	"VINICIUS-PC\SQLEXPRESS"; 		
			$connectionInfo = 	array( "Database"=>"walmart", "UID"=>"sa", "PWD"=>"m3scl4d0");
			$conn			=	sqlsrv_connect($serverName, $connectionInfo);
			
		} else 
		{
		
			$serverName 	= 	"187.18.58.83"; 	   
			$connectionInfo =	array( "Database"=>"05226_01", "UID"=>"hellofarma_adm", "PWD"=>"a6hP6b4jC1");		
			$conn 			= 	sqlsrv_connect($serverName, $connectionInfo);
			
		}
		
		$valores 	=	explode(',', $a);
		
		$strSQL		=	" SELECT * FROM mapas WHERE nome = ?";
		$pars		=	array($valores[0]);
		$stmt		=	sqlsrv_query($conn, $strSQL, $pars, $options) or die(print_r(sqlsrv_errors()));
		$obj		=	sqlsrv_fetch_object($stmt);
		
		
		$strSQL		=	" INSERT INTO ROTAS(mapa, origem, destino, distancia) values(?, ?, ?, ?) ";
		$pars		=	array($obj->id, $valores[1], $valores[2], $valores[3]);
		
		$stmt		=	sqlsrv_query($conn, $strSQL, $pars, $options) or die(print_r(sqlsrv_errors()));
		
		return 'sucesso';
		
	}
	///  função utilizada para calcular a rota
	function calculaRota($a)
	{
		
		$options 		=	array( "Scrollable" => SQLSRV_CURSOR_KEYSET );

		if ($_SERVER['SERVER_NAME'] == 'localhost') 
		{
		
			$serverName 	= 	"VINICIUS-PC\SQLEXPRESS"; 		
			$connectionInfo = 	array( "Database"=>"walmart", "UID"=>"sa", "PWD"=>"m3scl4d0");
			$conn			=	sqlsrv_connect($serverName, $connectionInfo);
			
		} else 
		{
		
			$serverName 	= 	"187.18.58.83"; 	   
			$connectionInfo =	array( "Database"=>"05226_01", "UID"=>"hellofarma_adm", "PWD"=>"a6hP6b4jC1");		
			$conn 			= 	sqlsrv_connect($serverName, $connectionInfo);
			
		}
		
		$valores 	=	explode(';', $a);
		
		$strSQL		=	" SELECT * FROM mapas WHERE nome = ?";
		$pars		=	array($valores[0]);
		$stmt		=	sqlsrv_query($conn, $strSQL, $pars, $options) or die(print_r(sqlsrv_errors()));
		$obj		=	sqlsrv_fetch_object($stmt);
	
		// definicao das variaveis do calculo
		$mapa			=	$obj->id;
		$origem			=	$valores[1];
		$destino		=	$valores[2];
		$autonomia		=	$valores[3];
		$valorLitro		=	str_replace(',', '.', $valores[4]);
		
		$strSQL			=	"SELECT * FROM rotas where destino = ? and mapa = ? ";
		$pars			=	array($destino, $mapa);
		
		$stmt			=	sqlsrv_query($conn, $strSQL, $pars, $options) or die(print_r(sqlsrv_errors()));
		
		$rotas				=	array();
		$rotaAlternativa	=	array();
		$preOrigem			=	array();
		
		/// agrega consulta de todas as rotas com valor de destino definido.
		while($obj	=	sqlsrv_fetch_object($stmt))
		{
			array_push($preOrigem, $obj->origem );
		}
		
		// algoritimo determina que para cada origem relacionada às rotas com destino definido verifiquem se coincidem com a origem definida pelo usuario.
		// agrega rotas especificas para caso a origem definida seja igual a origem relacionada ao destino definido pelo usuario.
		// reincide a verificao de origem caso a origem relacionada ao destino definido nao seja igual a origem definida pelo usuario.
		// calcula rotas de 2, 3 ou 4 pontos.
	
		while (count($preOrigem)  > 0)
		{
			$antOrigem	=	array_pop($preOrigem);
			
			if ($antOrigem	!=	$origem) 
			{
				
				$str		=	"	SELECT * FROM rotas 
								WHERE destino = ?	
								AND mapa = ?";
				
				$a			=	array($antOrigem, 1);
				$st			=	sqlsrv_query($conn, $str, $a, $options) or die(print_r(sqlsrv_errors()));
				
				while ($o	=	sqlsrv_fetch_object($st)) 
				{
					
					//echo '<br/>';
					if ($o->origem	==	$origem)
					{	
						array_push($rotas, array($origem, $antOrigem, $destino));
						
					} else 
					{
					
						if (in_array(array($antOrigem, $destino), $rotaAlternativa) == '')
						{
							$rotaAlternativa[$o->origem]	=	array($antOrigem, $destino);
						}
						
					}
					
				}
				
		
			} else 
			{
				array_push($rotas, array($origem, $destino));
			}
			
		}
		
		// caso seja encontrada rota de 4 pontos por exemplo 
		// a rotina verifica origens em funcao dos destinos definidos pela pequisa de rota atribuidas aos arrays no algoritimo anterior.
		// ate que seja encontrado uma origem igual a definida pelo usuario.
		// deve-se ser aprimorada para verificao de rotas de mais de 4 pontos.
		if (count($rotaAlternativa) > 0) 
		{
			
			foreach($rotaAlternativa as $key => $value) 
			{
				
				$str		=	"	SELECT * FROM rotas 
								WHERE destino = ? 
								AND mapa = ? ";
				
				$a			=	array($key, 1);
				$st			=	sqlsrv_query($conn, $str, $a, $options) or die(print_r(sqlsrv_errors()));
				
				while ($o	=	sqlsrv_fetch_object($st)) 
				{
					if ($o->origem	==	$origem)
					{	
						$strRota		=	'';
						$novaRota		=	array();
						
						array_push($novaRota, $origem);
						array_push($novaRota, $key);
						
						for ($i	= 0; $i < count($rotaAlternativa[$key]); $i ++)
						{	
							array_push($novaRota, $rotaAlternativa[$key][$i]);
							
						}
						
						array_push($rotas, $novaRota);
						
					} 
				}
				
			
				
			}
		
		}
	
		$distancias	=	array();
		$pontosRota	=	array(3, 2);
		
		/// atraves das rotas definidas pelas consultas anteriores de origem e destino calcula-se novamente por um nova consulta os valores de distancia
		foreach($rotas as $key=>$value)
		{
			if (count($rotas[$key])	>	3)
			{
			
				$distancia	=	0;
			
				for ($i = 0; $i < count($rotas[$key])-1; $i++)
				{
				
					$strSQL		=	" SELECT * FROM rotas 
								WHERE origem = ? 
								AND destino = ? 
								AND mapa = ? ";
								
					$a			=	array($rotas[$key][$i], $rotas[$key][$i+1], 1);
					$st			=	sqlsrv_query($conn, $strSQL, $a, $options) or die(print_r(sqlsrv_errors()));
					
					$obj        =   sqlsrv_fetch_object($st);

					$distancia	=	$distancia + $obj->distancia;
					
				}
				
				$distancias[$key]   =  intval($distancia);
				
			}
			else if (count($rotas[$key]) == 3)
			{	
				$distancia	=	0;
				
				$strSQL		=	" SELECT * FROM rotas 
								WHERE origem = ? 
								AND destino = ? 
								AND mapa = ? ";
								
				$a			=	array($rotas[$key][1], $rotas[$key][2], 1);
				$st			=	sqlsrv_query($conn, $strSQL, $a, $options) or die(print_r(sqlsrv_errors()));
				
				$obj        =   sqlsrv_fetch_object($st);

				$distancia	=	$distancia + $obj->distancia;
			
				$strSQL		=	" SELECT * FROM rotas 
								WHERE origem = ? 
								AND destino = ?
								AND mapa = ?";
								
				$a			=	array($rotas[$key][0], $rotas[$key][1], 1);
				$st			=	sqlsrv_query($conn, $strSQL, $a, $options) or die(print_r(sqlsrv_errors()));
				$obj        =   sqlsrv_fetch_object($st);
				
				$distancia	=	$distancia + $obj->distancia;
			
				$distancias[$key]   =  intval($distancia);
				
			} else 
			{
				
				$distancia	=	0;
				
				$strSQL		=	" SELECT * FROM rotas 
								WHERE origem = ? 
								AND destino = ? 
								AND mapa = ? ";
								
				$a			=	array($rotas[$key][0], $rotas[$key][1], 1);
				$st			=	sqlsrv_query($conn, $strSQL, $a, $options) or die(print_r(sqlsrv_errors()));
				$obj        =   sqlsrv_fetch_object($st);
				
				$distancia	=	$distancia + $obj->distancia;
			
				$distancias[$key]   =  intval($distancia);
				
			}
		}
		
		
		
		// determina a menor entre as distancias comparadas.
		
		$menorDistancia	 	=	0;
		$rota				=	'';
		
		foreach($distancias as $key => $value)
		{	
			if ($menorDistancia == 0) 
			{
				$menorDistancia	=	$distancias[$key];
				
			}
			if ($distancias[$key] < $menorDistancia)
			{
				$menorDistancia	=	$distancias[$key];
				
			}
		}
		
		
		/// localiza o indice da rota
		foreach($distancias as $key => $value) 
		{
			if ($distancias[$key] == $menorDistancia)
			{	
				$rota		=	$key;
			}
			
		}
		
		
		if (count($rotas) > 0)
		{
		
			
			$strRota		=	'';
			for ($i	= 0; $i < count($rotas[$rota]); $i ++)
			{	
				$strRota	.=	$rotas[$rota][$i].',';
			}
			
			
			$strRota		=		substr($strRota, 0, strlen($strRota) - 1);
		
			$distanciaAutonomia	=	$menorDistancia / $autonomia;
				
			$custo			=	$distanciaAutonomia * $valorLitro;
			
			$strSQL			=		" INSERT INTO requisicao(mapa, origem, destino, autonomia, valorLitro, rota, custo, distancia, dataIn) 
									VALUES(?, ?, ?, ?, ?, ?, ?, ?, convert(datetime, ?, 111)) ";
			$distanciaAutonomia	=	$menorDistancia / $autonomia;
				
			$custo		=	$distanciaAutonomia * $valorLitro;
				
				
			$pars		=		array($mapa, $origem, $destino, $autonomia, $valorLitro, $strRota, $custo, $menorDistancia, date("Y-m-d H:i:s"));
			$stmt		=	sqlsrv_query($conn, $strSQL, $pars, $options) or die(print_r(sqlsrv_errors()));
				
			return 'sucesso';
		
		} else 
		{
			return 'erro';
		}
	
	}
	///carega as requisicoes utilizadas no sistema.
	function carregaRequisicoes($a)
	{
		
		$options 		=	array( "Scrollable" => SQLSRV_CURSOR_KEYSET );

		if ($_SERVER['SERVER_NAME'] == 'localhost') 
		{
		
			$serverName 	= 	"VINICIUS-PC\SQLEXPRESS"; 		
			$connectionInfo = 	array( "Database"=>"walmart", "UID"=>"sa", "PWD"=>"m3scl4d0");
			$conn			=	sqlsrv_connect($serverName, $connectionInfo);
			
		} else 
		{
		
			$serverName 	= 	"187.18.58.83"; 	   
			$connectionInfo =	array( "Database"=>"05226_01", "UID"=>"hellofarma_adm", "PWD"=>"a6hP6b4jC1");		
			$conn 			= 	sqlsrv_connect($serverName, $connectionInfo);
			
		}
		
		$strSQL			=	"SELECT * FROM mapas WHERE nome = ? ";
		$pars			=	array($a);
		$stmt			=	sqlsrv_query($conn, $strSQL, $pars, $options) or die(print_r(sqlsrv_errors()));
		$obj			=	sqlsrv_fetch_object($stmt);
		
		
		$strSQL			=	"SELECT * FROM requisicao where mapa = ?";
		
		$pars			=	array($obj->id);
		$stmt			=	sqlsrv_query($conn, $strSQL, $pars, $options) or die(print_r(sqlsrv_errors()));
		
		
		$retorno		=	'';
		
		while ($obj		=	sqlsrv_fetch_object($stmt))
		{
			$retorno	.=	$obj->origem.' | '.$obj->destino.' | '.$obj->autonomia.' | '.$obj->valorLitro.' | '.str_replace(',', ' ', $obj->rota).' | '.$obj->custo.' | '.$obj->distancia.',';
		}
		
		$retorno		=	substr($retorno, 0, strlen($retorno) - 1);
		
		return $retorno;	
		
	}
	
	$HTTP_RAW_POST_DATA	=	isset($HTTP_RAW_POST_DATA) ?	$HTTP_RAW_POST_DATA	 : '';
	$server->service($HTTP_RAW_POST_DATA);
	
	
?>