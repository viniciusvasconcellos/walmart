<?php
	
	date_default_timezone_set("America/Sao_Paulo");
	
	$options 		=	array( "Scrollable" => SQLSRV_CURSOR_KEYSET );


	if ($_SERVER['SERVER_NAME'] == 'localhost') 
	{
	
		$serverName 		= 	"VINICIUS-PC\SQLEXPRESS"; 		
		$connectionInfo 	= 	array( "Database"=>"walmart", "UID"=>"sa", "PWD"=>"m3scl4d0");
		$conn				=	sqlsrv_connect($serverName, $connectionInfo);
		
	} else 
	{
	
		$serverName 		= 	"187.18.58.83"; 	   
		$connectionInfo	 	=	array( "Database"=>"05226_01", "UID"=>"hellofarma_adm", "PWD"=>"a6hP6b4jC1");		
		$conn 				= 	sqlsrv_connect($serverName, $connectionInfo);
		
	}
	
	// definicao das variaveis do calculo
	$origem 		=	'a';
	$destino		=	'e';
		
	$rotas			    =	array();
	$rotaAlternativa	=	array();
	$preOrigem			=	array();
	
	$strSQL			=	"	SELECT * FROM rotas
						where destino = ? 
						and mapa = ? ";
						
	$pars			=	array($destino, 1);
	
	$stmt			=	sqlsrv_query($conn, $strSQL, $pars, $options) or die(print_r(sqlsrv_errors()));
	
	/// agrega consulta de todas as rotas com valor de destino definido.
	while($obj	=	sqlsrv_fetch_object($stmt))
	{
		array_push($preOrigem, $obj->origem );
	}
	
	//print_r($preOrigem);
	
	// algoritimo determina que para cada origem relacionada às rotas com destino definido verifiquem se coincidem com a origem definida pelo usuario.
	// agrega rotas especificas para caso a origem definida seja igual a origem relacionada ao destino definido pelo usuario.
	// reincide a verificao de origem caso a origem relacionada ao destino definido nao seja igual a origem definida pelo usuario.
	// calcula rotas de 2, 3 ou 4 pontos.
	
	while (count($preOrigem)  > 0)
	{
		$antOrigem	=	array_pop($preOrigem);
		
		if ($antOrigem	!=	$origem) 
		{
			
			$str		=	"	SELECT * FROM rotas 
							WHERE destino = ? and mapa = ? ";
			
			$a			=	array($antOrigem, 1);
			$st			=	sqlsrv_query($conn, $str, $a, $options) or die(print_r(sqlsrv_errors()));
			
			while ($o	=	sqlsrv_fetch_object($st)) 
			{
				
				//echo '<br/>';
				if ($o->origem	==	$origem)
				{	
					array_push($rotas, array($origem, $antOrigem, $destino));
					
				} else {
				
					if (in_array(array($antOrigem, $destino), $rotaAlternativa) == '')
					{
						$rotaAlternativa[$o->origem]	=	array($antOrigem, $destino);
					}
					
				}
				
			}
			
	
		} else 
		{
			array_push($rotas, array($origem, $destino));
		}
		
		//echo '<br/>';
		
	}
	
	// caso seja encontrada rota de 4 pontos por exemplo 
	// a rotina verifica origens em funcao dos destinos definidos pela pequisa de rota atribuidas aos arrays no algoritimo anterior.
	// ate que seja encontrado uma origem igual a definida pelo usuario.
	// deve-se ser aprimorada para verificao de rotas de mais de 4 pontos.
	
	if (count($rotaAlternativa) > 0) 
	{
		
		foreach($rotaAlternativa as $key => $value) 
		{
			
			$str		=	"	SELECT * FROM rotas 
							WHERE destino = ? AND mapa = ? ";
			
			$a			=	array($key, 1);
			$st			=	sqlsrv_query($conn, $str, $a, $options) or die(print_r(sqlsrv_errors()));
			
			while ($o	=	sqlsrv_fetch_object($st)) 
			{
				if ($o->origem	==	$origem)
				{	
					$strRota		=	'';
					$novaRota		=	array();
					
					array_push($novaRota, $origem);
					array_push($novaRota, $key);
					
					for ($i	= 0; $i < count($rotaAlternativa[$key]); $i ++)
					{	
						array_push($novaRota, $rotaAlternativa[$key][$i]);
						
					}
					
					array_push($rotas, $novaRota);
					
				} 
			}
			
		
			
		}
	
	}
	

	
	$distancias	=	array();
	$pontosRota	=	array(3, 2);
	
		/// atraves das rotas definidas pelas consultas anteriores de origem e destino calcula-se novamente por um nova consulta os valores de distancia
		foreach($rotas as $key=>$value)
		{
		
			if (count($rotas[$key])	>	3)
			{
			
				$distancia	=	0;
			
				for ($i = 0; $i < count($rotas[$key])-1; $i++)
				{
				
					
					$strSQL		=	" SELECT * FROM rotas 
								WHERE origem = ? 
								AND destino = ? 
								AND mapa = ? ";
								
					$a			=	array($rotas[$key][$i], $rotas[$key][$i+1], 1);
					$st			=	sqlsrv_query($conn, $strSQL, $a, $options) or die(print_r(sqlsrv_errors()));
					
					$obj        =   sqlsrv_fetch_object($st);

					$distancia	=	$distancia + $obj->distancia;
					
				}
				
				$distancias[$key]   =  intval($distancia);
				
			}
			else if (count($rotas[$key]) == 3)
			{	
				$distancia	=	0;
				
				$strSQL		=	" SELECT * FROM rotas 
								WHERE origem = ? 
								AND destino = ? 
								AND mapa = ? ";
								
				$a			=	array($rotas[$key][1], $rotas[$key][2], 1);
				$st			=	sqlsrv_query($conn, $strSQL, $a, $options) or die(print_r(sqlsrv_errors()));
				
				$obj        =   sqlsrv_fetch_object($st);

				$distancia	=	$distancia + $obj->distancia;
			
				$strSQL		=	" SELECT * FROM rotas 
								WHERE origem = ? 
								AND destino = ? 
								AND mapa = ? ";
								
				$a			=	array($rotas[$key][0], $rotas[$key][1], 1);
				$st			=	sqlsrv_query($conn, $strSQL, $a, $options) or die(print_r(sqlsrv_errors()));
				$obj        =   sqlsrv_fetch_object($st);
				
				$distancia	=	$distancia + $obj->distancia;
			
				$distancias[$key]   =  intval($distancia);
				
			} else 
			{
				
				$distancia	=	0;
				
				$strSQL		=	" SELECT * FROM rotas 
								WHERE origem = ? 
								AND destino = ? 								
								AND mapa = ? ";
								
				$a			=	array($rotas[$key][0], $rotas[$key][1], 1);
				$st			=	sqlsrv_query($conn, $strSQL, $a, $options) or die(print_r(sqlsrv_errors()));
				$obj        =   sqlsrv_fetch_object($st);
				
				$distancia	=	$distancia + $obj->distancia;
			
				$distancias[$key]   =  intval($distancia);
				
			}
		}
		
		
		// determina a menor entre as distancias comparadas.
		$menorDistancia	 	=	0;
		$rota				=	'';
		
		foreach($distancias as $key => $value)
		{	
			if ($menorDistancia == 0) 
			{
				$menorDistancia	=	$distancias[$key];
				
			}
			if ($distancias[$key] < $menorDistancia)
			{
				$menorDistancia	=	$distancias[$key];
				
			}
		}
		
		
		/// localiza o indice da rota
		foreach($distancias as $key => $value) 
		{
			if ($distancias[$key] == $menorDistancia)
			{	
				$rota		=	$key;
			}
			
		}
		
		echo 'Origem: ';
		echo $origem.'<br/>';
		echo 'Destino: ';
		echo $destino.'<br/>';
		
		if (count($rotas) > 0)
		{
			echo 'Rotas: ';
			print_r($rotas);
			echo '<br/>';
			echo 'Distancias: ';
			print_r($distancias);
			echo '<br/>';			
			echo 'Menor Distancia: '.$menorDistancia;
			echo '<br/>';
			echo 'Indice Rota: '.$rota;
			echo '<br/>';
			echo 'Melhor Rota: ';
			print_r($rotas[$rota]);
			
			$strRota		=	'';
			for ($i	= 0; $i < count($rotas[$rota]); $i ++)
			{	
				$strRota	.=	$rotas[$rota][$i].',';
			}
			
			
			$strRota		=		substr($strRota, 0, strlen($strRota) - 1);

			$strSQL		=		" INSERT INTO requisicao(mapa, origem, destino, autonomia, valorLitro, rota, custo, distancia, dataIn) 
								VALUES(?, ?, ?, ?, ?, ?, ?, ?, convert(datetime, ?, 111)) ";
			$mapa		=		1;
			$autonomia	=		10;
			$valorLitro	=		str_replace(',', '.', '2,5');
			
			$distanciaAutonomia	=	$menorDistancia / $autonomia;
			
			$custo		=	$distanciaAutonomia * $valorLitro;
			
			
			$pars		=		array($mapa, $origem, $destino, $autonomia, $valorLitro, $strRota, $custo,$menorDistancia, date("Y-m-d H:i:s"));
			$stmt		=	sqlsrv_query($conn, $strSQL, $pars, $options) or die(print_r(sqlsrv_errors()));
			
		} else 
		{
			echo "Rota nao encontrada";
			
		}
		
?>